function [Part, XY, Idx, P_d] = Wvlt_Tracking(A,J)
% Wavelet Particle Tracking
% Detects particles in image stack A via a multiscale wavelet algorithm and
% connects particle positions between frames using a nearest neighbor
% approach. 
% A = Image stack
% J = Optional input of wavelet depth
%
% Part = Particle array with (x,y,t) triplets
% XY = Cellarray with particle positions per frame
% Idx = Cellarray of particles with indices per frame (Linking, no XY positions)
% Stats = Global settings
% P_d = Cellarray of particle mask per frame (optional)
% � P. Messer, 2016
%% General
global Wvlt

narginchk(1,2);

if ~exist('J','var') || ~isnumeric(J)
    J = 6; %preallocate number of approximations
end

Wvlt.A = A; 
XY = cell(size(A,3),1); %allocating particle positions
P_d = cell(size(A,3),1); %allocating particle detection mask per frame

h = Wvlt_Thr_Adjustment(J); %set parameters in GUI
uiwait(h);

J = Wvlt.J; % Number of Details
tol = Wvlt.T; % Absolute distance threshold between subsequent frames (in px)
fskip = Wvlt.FS; % Frame Skip distance

%% Position Detection via Wavelet Anlysis
for i = 1:size(A,3)
    [XY{i},P_d{i},~] = Wvlt_Partfind(double(A(:,:,i)),J,i);
end

% Convert per-frame cell to per-particle cell and connect particles to trajectories
out = cellfun(@(x) horzcat(x(1:2),1,NaN),mat2cell(XY{1},ones(size(XY{1},1),1),2),'UniformOutput',false);
Idx = num2cell([(1:numel(out))',ones(numel(out),1)],2);

%% Link Particle Positions via Euclidean Distance
for i = 2:numel(XY)
    t_idx = cell2mat(cellfun(@(x)(i-x(end,3))<=fskip,out,'UniformOutput',false));
    tout = out(t_idx);
    ttest = Idx(t_idx);
    [D,I] = pdist2(cell2mat(cellfun(@(x)x(end,1:2),tout,'UniformOutput',false)),XY{i}(:,1:2),'euclidean','Smallest',1);
    d = (D(:)<=tol); % Distances below tolerance (true successive particles)
    D = D';
    I = I';
    % Check for dublicates if no Particle is in range. this has led to
    % missing particles if both are assigned to the same previous particle
    if any(d)
        [N,E] = histcounts(I(d),[unique(I(d));Inf]);
        E = E(1:end-1);
        doubleI = E(logical(N-1));
        for ii = 1:numel(doubleI)
            ex_idx = find(I==doubleI(ii)&d); % current frame particle indices
            comD = D(ex_idx); % get minimal distances
            mn = min(comD); % get smaller distance
            r = regionprops(P_d{i},'area');
            tsz = [r(ex_idx).Area];
            amax = max(tsz);
            %         d(ex_idx(comD~=mn)) = 0;
            d(ex_idx(tsz~=amax)) = 0;
        end
    end
    % Add new positions to trajectories based on distance
    if any(d)
        tout(I(d)) = arrayfun(@(x,y,z,a) vertcat(tout{x},[y z i a]),I(d),XY{i}(d,1),XY{i}(d,2),D(d),'UniformOutput',false);
        ttest(I(d)) = arrayfun(@(x,y) vertcat(ttest{x},[y,i]),I(d),find(d),'UniformOutput',false);
        if ~all(eq(1:size(XY{i},1),sort([find(d)',find(~ismember(1:size(XY{i},1),find(d)))])))
            error('Particles were lost during concatenation')
        end    
        Idx(t_idx) = ttest; 
    end
    Idx = [Idx;num2cell([find(~d),repmat(i,sum(~d),1)],2)]; %add non-linked positions as new starting points
    out(t_idx) = tout;
    out = [out;cellfun(@(x) horzcat(x(1:2),i,NaN),mat2cell(XY{i}(~d,1:2),ones(size(XY{i}(~d,1:2),1),1),2),'UniformOutput',false)];
    if ~all(cellfun(@(x,y)eq(size(x,1),size(y,1)),out,Idx))
        error('Cell structures are not equal in size. Frames were lost.')
    end
end

ti = 1;
for i = 1:numel(Idx)
        for ii = 1:size(Idx{i},1)
            Part{ti}(ii,:) = [XY{Idx{i}(ii,2)}(Idx{i}(ii,1),:),Idx{i}(ii,2)];
        end
        ti = ti+1;
end
Wvlt = rmfield(Wvlt,{'A';'w'});
assignin('base','Stats',Wvlt)
end