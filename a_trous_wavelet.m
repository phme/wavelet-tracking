function [w,A] = a_trous_wavelet(A,J,k)
% Creates the wavelets w of an image A to depth J-1, 
% where A(:,:,J) are the smoothed approximations of the original input
% Insignificant coefficients in w are set to zero if <k*MAD
% � P. Messer, 2016

narginchk(1,3)
if nargin==2
    k = 2; % Set k if not specified before
end
w = zeros(size(A));

for i = 1:J-1
    
    kern = [1/16,zeros(1,2^(i-1)-1),1/4,zeros(1,2^(i-1)-1),3/8,zeros(1,2^(i-1)-1),1/4,zeros(1,2^(i-1)-1),1/16]; % Convolution Kernel (a trous)
    kernsize = numel(kern);
    t = conv2(kern,kern,padarray(A(:,:,i),[kernsize kernsize],'symmetric'),'same'); % Image gets symmetrical padded to prevent edge effects
    A(:,:,i+1) = t(kernsize+1:end-kernsize,kernsize+1:end-kernsize); % Crop image to original size
    tw = A(:,:,i)- A(:,:,i+1); % Get Wavelet
    mad = abs(tw - median(tw(:))); % Calculate Median Absolute Deviation (MAD)
    sig(i) = k*median(mad(:)); % Set significance level
    tw(tw<sig(i)) = 0; % Remove insignificant wavelet coefficients
    w(:,:,i) = tw;
end