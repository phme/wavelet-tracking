function [XY,P_d,w] = Wvlt_Partfind(A,J,f)
% A = Image Plane (double)
% J = Number of Details (Depth of Wavelet Transformation)
% XY = Cell array including particle positions
% P_d = Boolean array of Chosen Pixels
% w = Wavelet Array
% � P. Messer, 2016
global Wvlt

%% Calculate wavelets
[w,A] = a_trous_wavelet(A,J,Wvlt.k);

%% Apply global threshold
switch Wvlt.Method
    case 'sum'
        P = sum(w(:,:,Wvlt.Depth),3);
        [N,E] = histcounts(P);
    case 'prod'
        P = prod(w(:,:,Wvlt.Depth),3);
        [N,E] = histcounts(log10(P(P~=0)),linspace(log10(min(P(P~=0))),log10(max(P(P~=0))),25));
end
C = cumsum(N(2:end)); C = C./max(C);
P_str(f).N = N; P_str(f).E = E; P_str(f).C = C;
P_str(f).l = find(C>=Wvlt.CutOff,1,'first'); % Use only the most significant residues (0.5%)
P_d = false(size(P));
if max(log10(P(:)))<1
    P_d(P>10^E(P_str(f).l)) = 1;
else
    P_d(abs(P)>10^(E(P_str(f).l))) = 1; % Create boolean array
end
% Remove single pixel areas
P_d = bwmorph(P_d,'close');

%% Identify particles
e = regionprops(P_d,'Eccentricity','Area');
%     tplane = ismember(bwlabel(P_d),find([e.Eccentricity]<0.75));
P_d = ismember(bwlabel(P_d),find([e.Area]>4)); %Only keep particle positions with at least 5 pixels

STATS = regionprops(P_d, 'area','Centroid','Eccentricity'); % Find regions with high correlating coefficients
if isempty(STATS)
    display('No Particles found');
    XY = [];
    return
end
XY = cell2mat(arrayfun(@(x) [x.Centroid(1),x.Centroid(2)],STATS,'UniformOutput',0)); % Extract particle positions

%% Remove particles to close to image borders
delmax =  round(XY(:,2))+2>size(A,1) | round(XY(:,1))+2>size(A,2);
delmin = delmax | round(XY(:,2))-2<1 | round(XY(:,1))-2<1;
if any(delmin)
    XY(delmin,:) = [];
    for i = find(delmin)'
        P_d(bwlabel(P_d)==i) = 0;
    end
end

del = find(isnan(XY(:,1))); % NaN should always appear in both coordinates
if ~isempty(del)
    XY(del,:) = [];
end

%% Check particle distance and remove nearby particles
D = tril(squareform(pdist(XY,'euclidean'))); % lower triangular distance matrix -> only onedirectional distances to avoid dublicates
[xx,yy] = ind2sub(size(D),find(D<Wvlt.T&D~=0)); % find Distances smaller than 7 pixels (and exclude diagonal)
cpart_idx = [xx,yy]; ii=0;

for i = unique(xx)' % Go through all particles that have a close particle nearby
    ii = ii+1;
    [~,tx] = ismember(i,xx);
    conf_part{ii} = cpart_idx(tx,:);
    [~,tx] = ismember(conf_part{ii},yy); % find particles on the "other side" to find triplets
    conf_part{ii} = [conf_part{ii};cpart_idx(tx(tx~=0),:)];
    conf_part{ii} = unique(conf_part{ii}); % keep only one index per particle
end

idx = ones(size(XY,1),1); % create index to exclude particles
idx(unique(cpart_idx)) = 0; % set all clustered particles to 0
r = 1; % intensity radius
warning('off','MATLAB:colon:nonIntegerIndex') % get rid of stupid warning
if exist('conf_part','var')
    for i = 1:numel(conf_part) % Loop over conflicting clusters
        pa = XY(conf_part{i},:);
        % Calculate sum intensity for each particle in cluster and choose the brightest
        for ii = 1:size(pa,1)
            try
                int_sum{i}(ii) = sum(sum((A(pa(ii,2)-r:pa(ii,2)+r,pa(ii,1)-r:pa(ii,1)+r,1))));
            catch %particle to close to the border
                int_sum{i}(ii) = 0;
            end
        end
        [~,mi] = max([STATS(conf_part{i}).Area]);
        idx(conf_part{i}(mi)) = 1; % include particle
    end
end
ex = find(~idx);
for i = numel(ex):-1:1
    XY(ex(i),:) = [];
    P_d(bwlabel(P_d)==ex(i)) = 0;
end
XY = reshape(XY,[],2);
XY = unique(XY,'rows'); % somehow it can happen that there is more than one particle in a location

