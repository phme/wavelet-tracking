# Wavelet Tracking

Powerful algorithm to track single particles in low SNR environments.

This is a MATLAB based SPT program, based on a multiscale wavelet transform algorithm.
It detects particle positions in 2D arrays and link them between subsequent frames via a nearest neighbor approach.

This work is based on:
Olivo-Marin, Extraction of spots in biological images using multiscale products. 2002